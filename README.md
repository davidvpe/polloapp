# PolloApp

It might sound a little weird to you, but it's spanish for Chicken-App, yes your heard it right, **CHICKEN**.

In Peru, that beautifull land where this awesome team come from, is well known by it's food and one dish in particular is called **Pollo a la Brasa** 🤤. It might not look like a big deal for you but for us finding the best Pollo a la Brasa in the city is one of the most exciting and delicious things you can ever do.

So we decided to make the PolloApp, where you'll be able to search for the best places where you can eat this, also known as **Pollerias**, also if you know other places that are not here yet, feel free to submit an _Add Request_ and we'll check, and eat it out, to see if it's worth of being in the Pollo App.

Ah yeah, almost forgot, here is a pic:

![pollo]

[pollo]: https://img.elcomercio.pe/files/ec_article_multimedia_gallery/uploads/2017/07/14/596902044ea9e.jpeg "Pollo a la brasa"
