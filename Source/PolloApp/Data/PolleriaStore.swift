//
//  PolleriaStore.swift
//  PolloApp
//
//  Created by Velarde Robles, David on 10/06/2019.
//  Copyright © 2019 David Velarde. All rights reserved.
//

import SwiftUI
import Combine

class PolleriaStore: BindableObject {

    var didChange = PassthroughSubject<Void, Never>()

    var pollerias = [Polleria]() {
        didSet {
            didChange.send(Void())
        }
    }

    var isLoading: Bool = false {
        didSet {
            didChange.send(Void())
        }
    }

    init() {
        startLoading()
    }

    func startLoading() {

        isLoading = true

        guard let url = URL(string: "https://www.mocky.io/v2/5cfdf6cf320000520045ed3e") else {
            return
        }

        NetworkManager.shared.fetch(fromURL: url) { data in
            guard let data = data else {
                self.pollerias = [Polleria]()
                return
            }
            self.pollerias = (try? JSONDecoder().decode([Polleria].self, from: data)) ?? [Polleria]()
            self.isLoading = false
        }
    }

}
