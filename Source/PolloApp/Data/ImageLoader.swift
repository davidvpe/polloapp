//
//  ImageLoader.swift
//  PolloApp
//
//  Created by Velarde Robles, David on 09/06/2019.
//  Copyright © 2019 David Velarde. All rights reserved.
//

import SwiftUI
import Combine

class ImageLoader: BindableObject {

    var didChange = PassthroughSubject<Data, Never>()

    var data = Data() {
        didSet {
            didChange.send(data)
        }
    }

    init(imageURL: String) {
        guard let url = URL(string: imageURL) else {
            return
        }

        NetworkManager.shared.download(withURL: url) { data in
            guard let data = data else {
                return
            }
            self.data = data
        }
    }
}
