//
//  Polleria.swift
//  PolloApp
//
//  Created by Velarde Robles, David on 09/06/2019.
//  Copyright © 2019 David Velarde. All rights reserved.
//

import SwiftUI

struct Polleria: Codable, Identifiable {
    let id: Int
    let name: String
    let imageURL: String
    let address: String
    var downloadedImage = UIImage()

    enum CodingKeys: String, CodingKey {
        case id
        case name
        case imageURL
        case address
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        imageURL = try values.decode(String.self, forKey: .imageURL)
        address = try values.decode(String.self, forKey: .address)
    }
}
