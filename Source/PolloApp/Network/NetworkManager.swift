//
//  NetworkManager.swift
//  PolloApp
//
//  Created by Velarde Robles, David on 09/06/2019.
//  Copyright © 2019 David Velarde. All rights reserved.
//

import Foundation

struct NetworkManager {

    static let shared = NetworkManager()

    private let session = URLSession.shared

    func download(withURL url: URL, completionHandler: @escaping ((Data?) -> Void)) {
        session.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                guard data != nil, error == nil else {
                    completionHandler(nil)
                    return
                }
                completionHandler(data)
            }
        }.resume()
    }

    func fetch(fromURL url: URL, completionHandler: @escaping ((Data?) -> Void)) {
        session.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                guard data != nil, error == nil else {
                    completionHandler(nil)
                    return
                }
                completionHandler(data)
            }
            }.resume()
    }
}
