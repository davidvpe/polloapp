//
//  ContentView.swift
//  PolloApp
//
//  Created by Velarde Robles, David on 09/06/2019.
//  Copyright © 2019 David Velarde. All rights reserved.
//

import SwiftUI
import Combine

struct HomeView : View {

    @ObjectBinding var store: PolleriaStore

    var body: some View {
        ZStack {
            if store.isLoading {
                ActivityIndicator(style: .large)
            } else {
                List(store.pollerias) { polleria in
                    PolleriaListItem(polleria: polleria)
                }
            }
        }
    }
}

#if DEBUG
struct HomeView_Previews : PreviewProvider {
    static var previews: some View {
        Text("Hola")
    }
}
#endif
