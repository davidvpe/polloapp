//
//  PolleriaListItem.swift
//  PolloApp
//
//  Created by Velarde Robles, David on 10/06/2019.
//  Copyright © 2019 David Velarde. All rights reserved.
//

import SwiftUI

struct PolleriaListItem: View {

    var polleria: Polleria

    var body: some View {
        ZStack(alignment: .bottom) {
            NetworkImage(imageURL: polleria.imageURL)
                .cornerRadius(6.0)
                .aspectRatio(contentMode: .fit)
                .layoutPriority(1.0)
//            Rectangle().fill(LinearGradient(
//                gradient: .init(colors: [Color.clear, Color.black]),
//                startPoint: .init(x: 0, y: 0.5),
//                endPoint: .init(x: 0, y: 1)
//            ))
            Text(polleria.name)
                .font(.title)
                .padding()
                .background(Color.yellow)
                .cornerRadius(6.0)
                .padding()
        }
    }
}
