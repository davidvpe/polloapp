//
//  NetworkImage.swift
//  PolloApp
//
//  Created by Velarde Robles, David on 09/06/2019.
//  Copyright © 2019 David Velarde. All rights reserved.
//

import SwiftUI

struct NetworkImage: View {

    @ObjectBinding var imageLoader: ImageLoader

    init(imageURL: String) {
        imageLoader = ImageLoader(imageURL: imageURL)
    }

    var body: some View {
        Image(uiImage: getUIImage()).resizable()
    }

    func getUIImage() -> UIImage {
        guard imageLoader.data.count > 0, let image = UIImage(data: imageLoader.data) else {
            return UIImage()
        }
        return image
    }
}
